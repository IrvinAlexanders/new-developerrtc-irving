## RTFM and LMGTFY
Although I have not specifically used the acronyms RTFM and LMGTFY, I can assure you that I have a strong ability to solve problems independently and efficiently. My approach to development is based on autonomy and the active search for solutions.  
Throughout my career, I have demonstrated abilities to approach challenges with creativity and determination, thus streamlining the development process. My ability to research, analyze and apply solutions effectively has allowed me to contribute significantly to the projects in which I have participated.  
I am committed to excellence and am confident that I can bring significant value to your team through my ability to independently solve problems.

## Operating systems that I master.
### Linux distributions:
- Ubuntu 20.04
- Fedora 35
- Debian 11
### Windows:
- Windows 10
- Windows 11

## Programming languages:
### Advanced level:
- Python3
- Ruby
### Intermediate level:
- C#
- C++
- JavaScript

## Frameworks:
### Advanced level:
- Django - Django RF
- Flask
- Pyramid
- Ruby on Rails
### Intermediate level:
- .NET Core
- Express Js

## Languages:
- Spanish (Native)
- English (B1)