def find_matching_pair(numbers, target_sum):
    left = 0
    right = len(numbers) - 1

    while left < right:
        current_sum = numbers[left] + numbers[right]

        # If the sum is equal to the target, we find the pair and return it
        if current_sum == target_sum:
            return numbers[left], numbers[right]
        # If the sum is less than the target, we move the left index finger to the right
        elif current_sum < target_sum:
            left += 1
        # If the sum is greater than the target, we move the right index finger to the left
        else:
            right -= 1

    # If no pair is found that sums to the objective, we return None
    return None

# Example:
numbers = [1,3,3,7]
target_sum = 9
pair = find_matching_pair(numbers, target_sum)
if pair:
    print(f"OK, matching pair {pair} for sum {target_sum}")
else:
    print("No matching pair found")