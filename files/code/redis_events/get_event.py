# # Utils
# import redis
# import logging

# # Locals
# from app.config.settings import ConfigDatabase
# from app.utils.aws import invoke_lambda
# from app.utils.functions import set_key_status, change_file_status
# from app.utils import constants
# from app.models.models import ScheduledBadge, BadgeFile
# from app.config import settings


# db = ConfigDatabase()

# def event_handler(msg):
#     try:
#         key = msg["data"].decode("utf-8")
#         logging.info(key)
#         if "_SHADOW" in key:
#             parts = key.split("_SHADOW")
#             if len(parts) == 2:
#                 original_name = parts[0]
#                 # Get the original Key
#                 value = cache.get(original_name).decode("utf-8") if cache.get(original_name) else None
#                 if value:
#                     cache.delete(original_name)
#                     cache.delete(key)

#                     file_id = int(value)
#                     change_file_status(db, file_id, BadgeFile.INITIAL)
                    
#                     invoke_lambda(data={'file_id': file_id})
#                     # Change the task status on the Database
#                     set_key_status(db, original_name, ScheduledBadge.PROCESSED)
#                     logging.info(f"Task '{original_name}' processed")
#                 else:
#                     # Change the task status on the Database
#                     set_key_status(db, original_name, ScheduledBadge.FAILED)
#                     raise ValueError()
                
#     except ValueError:
#         logging.error(constants.CACHE_VALUE_ERROR)

#     except IndexError:
#         logging.error(constants.CACHE_MESSAGE_ERROR)

#     except Exception as exp:
#         logging.error(exp)

# # Creating Redis and pubsub Connection

# cache = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT)
# pubsub = cache.pubsub()
# # Set config in config file "notify-keyspace-events Ex"
# # Subscribing to key expire events and whenver we get any notification sending it to event_handler function
# pubsub.psubscribe(**{"__keyevent@0__:expired": event_handler})
# pubsub.run_in_thread(sleep_time=0.01)
