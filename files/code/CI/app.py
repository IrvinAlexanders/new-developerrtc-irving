from flask import Flask, request, jsonify
import math
import os

app = Flask(__name__)

@app.route('/loan', methods=['POST'])
def calculate_loan_payment():
    data = request.json
    total_amount = data.get('total_amount')
    interest_rate = data.get('interest_rate')
    months = data.get('months')

    if total_amount is None or interest_rate is None or months is None:
        return jsonify({'error': 'Missing parameters'}), 400

    interest_rate /= 100  # convert interest rate from percentage to decimal
    monthly_interest = interest_rate / 12
    monthly_payment = (total_amount * monthly_interest) / (1 - math.pow(1 + monthly_interest, -months))

    return jsonify({'monthly_payment': round(monthly_payment, 2)}), 200

if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port, debug=True)
