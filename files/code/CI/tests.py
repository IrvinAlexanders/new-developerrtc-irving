import pytest
from .app import app

@pytest.fixture
def client():
    app.config['TESTING'] = True
    with app.test_client() as client:
        yield client

def test_calculate_loan_payment(client):
    # Caso de prueba para verificar si se calcula correctamente el pago mensual
    data = {
        'total_amount': 10000,
        'interest_rate': 5,
        'months': 12
    }
    response = client.post('/loan', json=data)
    assert response.status_code == 200
    assert response.json['monthly_payment'] == pytest.approx(856.07, 0.01)  # Verificar aproximadamente el pago mensual

def test_missing_parameters(client):
    # Caso de prueba para verificar si faltan parámetros
    data = {
        'total_amount': 10000,
        'interest_rate': 5
    }
    response = client.post('/loan', json=data)
    assert response.status_code == 400
    assert response.json['error'] == 'Missing parameters'